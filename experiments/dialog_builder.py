# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


from emzed_gui import DialogBuilder, show_information


def callback(data):
    show_information(f"the contet of the field labeled 'text' is {data.text}")
    data.abc = 123


dlg = (
    DialogBuilder()
    .add_instruction("instruction")
    .add_bool("bool")
    .add_int("int with slider", slider=True, min=-100, max=100)
    .add_int("int")
    .add_float("float with slider", slider=True, min=0, max=1)
    .add_float("float")
    .add_date_time("datetime")
    .add_date("date")
    .add_choice("choice", ["one", "two"])
    .add_multiple_choice("choice", ["one", "two", "three"])
    .add_text("text")
    .add_text("text not empty", notempty=True)
    .add_string("string")
    .add_string("string not empty", notempty=True)
    .add_file_open("input")
    .add_files_open("inputs")
    .add_file_save(
        "output",
        default="output.csv",
        basedir="/tmp",
        notempty=False,
        extensions=["csv"],
    )
    .add_directory("folder")
    .add_color("color")
    .add_button("press me", callback)
    .add_button("press me to", callback, check_other_fields_first=True)
)


data = dlg.show(defaults=dict(text="HI YO!1!!"))

if data is None:
    print("cancelled")

else:
    print("data.abc is", data.abc)
