SERVER_URL=sissource.ethz.ch:5005
REPOSITORY_URL=${SERVER_URL}/sispub/emzed/emzed_gui
TAG=${REPOSITORY_URL}/python-with-tox

docker build --squash -t ${TAG} .
docker run -ti ${TAG} /bin/bash
