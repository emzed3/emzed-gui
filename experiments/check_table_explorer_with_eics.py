# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import os

from emzed import Table, to_table, PeakMap
from emzed import run_feature_finder_metabo
from emzed.quantification.peak_integration import integrate
from emzed.ms2 import attach_ms2_spectra
from emzed.gui import inspect

PEAKS_IN = "experiments/peaks.db"
PEAKS_OUT = "experiments/peaks_integrated.db"

if not os.path.exists(PEAKS_IN):
    pm = PeakMap.load("experiments/test_smaller.mzXML")
    t = run_feature_finder_metabo(pm)
    attach_ms2_spectra(t)
    t.save(PEAKS_IN)

t = Table.load(PEAKS_IN)

if not os.path.exists(PEAKS_OUT):
    t = integrate(t[:1000], "emg", n_cores=1, xtol=1.49012e-08, gtol=0.0)
    t.save(PEAKS_OUT)

t = Table.load(PEAKS_OUT)

t.set_col_format("peakmap", "%s")
# t = t.join(t)

inspect(t)
