How to install emzed gui?
=========================

Please use a virtual environment to install emzed gui.

In case you did not install ``emzed`` already:

.. code-block:: bash

    $ pip install --extra-index-url https://pypi-sissource.ethz.ch/simple emzed3



Installation on Mac OS X
-------------------------

.. code-block:: bash

    $ pip install numpy Cython
    $ pip install PyQt5==5.11.2
    $ pip install guidata
    $ pip install guiqwt
    $ pip install --extra-index-url https://pypi-sissource.ethz.ch/simple emzed3_gui


Installation on Ubuntu 19.10
----------------------------

.. code-block:: bash

    $ pip install numpy Cython
    $ pip install PyQt5==5.9.2
    $ pip install guidata
    $ pip install guiqwt
    $ pip install --extra-index-url https://pypi-sissource.ethz.ch/simple emzed3_gui


Installation on Windows
-----------------------

.. code-block:: bash

    $ pip install numpy Cython
    $ pip install PyQt5==5.9.2
    $ pip install guidata
    $ pip install --extra-index-url https://pypi-sissource.ethz.ch/simple guiqwt
    $ pip install --extra-index-url https://pypi-sissource.ethz.ch/simple emzed3_gui
