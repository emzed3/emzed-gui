.. emzed gui documentation master file, created by
   sphinx-quickstart on Wed Jun 10 16:58:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to emzed gui's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
