#! /bin/sh
#
# upload_example_scripts.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

scp -r example_scripts emzed:htdocs/downloads
