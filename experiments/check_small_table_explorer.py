# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.



from emzed import Table, to_table, PeakMap
from emzed.gui import inspect


t0 = to_table("i", [1, 2, 3], int)

pm = PeakMap.load("experiments/ms1_and_ms2_mixed.mzML")

t = Table.create_table(
    ["i", "f", "s", "b", "t", "pm"],
    [int, float, str, bool, Table, PeakMap],
    rows=[
        [1, 2.0, "3", False, t0, pm],
        [None, 3.0, "4", True, t0, pm],
        [1, None, None, None, t0, pm],
        [1, 2.0, "4", True, t0, pm],
        [2, 3.0, "4", True, t0, pm],
        [2, 3.0, "4", True, t0, pm],
        [1, 2.0, "4", True, t0, pm],
        [7, 2.0, "4", True, t0, pm],
        [8, 2.0, "4", True, t0, pm],
        [9, 2.0, "4", True, t0, pm],
    ],
)

t.set_col_format("pm", "%s")

print(t)
inspect(t)
inspect(t)
inspect(t)
