# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import os

from emzed.lib import Table, to_table, PeakMap
from emzed.lib import run_feature_finder_metabo
from emzed.lib.ms2 import attach_ms2_spectra
from emzed.gui import inspect

PEAKS = "experiments/peaks.db"

if not os.path.exists(PEAKS):
    if 0:
        pm = PeakMap.load("experiments/test_smaller.mzXML")
    else:
        pm = PeakMap.load("experiments/ms1_and_ms2_mixed.mzML")
    t = run_feature_finder_metabo(pm)
    attach_ms2_spectra(t)
    t.save(PEAKS)
else:
    t = Table.load(PEAKS)

# t = t.join(t)

def test_inspector():

    inspect(t)
