#
# requires: local venv with x86_64 Python 3.7
# also check the pathes below
#

python -m pip install -U pip

pip install 'numpy<1.23' matplotlib

pip install -e ../emzed

pip install Cython
pip install PyQt5==5.15.7
QT_API=pyqt5 pip install guidata==1.7.7
QT_API=pyqt5 pip install guiqwt==3.0.3
# pip install -e .
