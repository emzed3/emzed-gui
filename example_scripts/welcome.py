#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

from emzed.gui import ask_yes_no, show_information

while True:
    ok = ask_yes_no("do you like emzed?")
    if ok:
        break
    show_information("please try again")

print("I like your answer")
