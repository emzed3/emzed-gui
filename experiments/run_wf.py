# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.



from emzed.gui.dialog_builder import *


class Wf(WorkflowFrontend):

    breakpoint()
    plib = WorkflowFrontend().set_defaults()
    ident = WorkflowFrontend().set_defaults()
    mat = WorkflowFrontend().set_defaults()
    complete = RunJobButton(
        "merge and complete libraries", method_name="run_complete_library"
    )
    ident = RunJobButton(
        "build identification library", method_name="run_build_ident_lib"
    )
    mat = RunJobButton("build data_matrix", method_name="run_build_data_matrix")
    stats = RunJobButton("perform pairwise stats", method_name="run_pairwise_stats")

    def run_complete_library(self):
        pass

    run_build_ident_lib = (
        run_build_data_matrix
    ) = run_pairwise_stats = run_complete_library


Wf().show()
