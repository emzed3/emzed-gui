#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

from emzed import PeakMap
from emzed.gui import inspect

pm = PeakMap.load("test.mzXML")
inspect(pm)
