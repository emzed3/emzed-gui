# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


from emzed import PeakMap
from emzed.gui import inspect

# pm1 = PeakMap.load("experiments/ms1_and_ms2_mixed.mzML")
pm2 = PeakMap.load("experiments/test_smaller.mzXML")

print("loaded")
window = (1861.01089999998, 1917.84109999998, 800.0106201171875, 800.0247802734375)
window = (1861.01089999998, 1917.84109999998, 700.0106201171875, 900.0247802734375)
window = (0.05 * 60, 0.15 * 60, 300, 900)
#inspect(pm2, window=window)
inspect(pm2)
