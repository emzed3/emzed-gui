#!/bin/sh
#
# reformat_code.sh
# Copyright (C) 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

echo
echo RUN ISORT
echo
isort src tests

echo
echo RUN BLACK
echo
black src tests
