#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>


from emzed.gui import DialogBuilder, show_information


def algorithm_one(dlg):
    show_information(f"you want me to run algorithm 1 with tolerance {dlg.tolerance}")


def algorithm_two(dlg):
    show_information(f"you want me to run algorithm 2 with tolerance {dlg.tolerance}")


dlg = (
    DialogBuilder()
    .add_float("tolerance", slider=True, min=0, max=1)
    .add_button("run algorithm 1", algorithm_one)
    .add_button("run algorithm 2", algorithm_two)
)
dlg.show()

show_information("now the same dialog again, but with different buttons at the bottom")

dlg.show(ok_button="YES PLEASE", cancel_button=None)
