#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

from emzed.gui import inspect
from emzed import PeakMap, run_feature_finder_metabo


pm = PeakMap.load("test.mzXML")

peaks = run_feature_finder_metabo(
    pm, mtd_noise_threshold_int=5000, common_chrom_peak_snr=10
)

print()
print("DONE!")
print()
print("in case the peak table does not pop up: please check your task bar")

inspect(peaks)
