# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.



import sqlite3
import time
import random
from threading import Thread

db_name = "check"

uri = f"file:{db_name}?mode=memory&cache=shared"


class T(Thread):
    def __init__(self):
        self.conn = sqlite3.connect(uri, check_same_thread=False)
        print(self.conn)
        super().__init__()

    def run(self):
        print(self, "started")
        for i in range(10):
            print(self, i)
            values = [random.random(), random.random(), random.random()]
            try:
                self.conn.execute("INSERT INTO ABC VALUES (?, ?, ?)", values)
            except:
                print("XX", self.conn)
                raise
            self.conn.commit()
            time.sleep(random.random() * .1)
        self.conn.close()


conn = sqlite3.connect(uri, check_same_thread=False)
conn.execute("CREATE TABLE ABC (x FLOAT, y FLOAT, z FLOAT);")
conn.commit()
conn.close()

threads = [T() for _ in range(3)]
for t in threads:
    t.start()

print("STARTED")
for t in threads:
    t.join()

print("DONE")
