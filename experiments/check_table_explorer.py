# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import os
import random
import sys

import emzed
import matplotlib
from emzed import PeakMap, Table, run_feature_finder_metabo
from emzed.ms2 import attach_ms2_spectra
from emzed.quantification import integrate, integrate_chromatograms

from emzed_gui import inspect

PEAKS = "experiments/peaks.table"

pm = PeakMap.load("experiments/mrm_data.mzML")
t = emzed.peak_picking.extract_ms_chromatograms(pm)


def cut_chromatogram(c):
    return c.__class__(c.mz, c.precursor_mz, c.rts[:100], c.intensities[:100], c.type)


def minrt(c):
    return min(c.rts)


def maxrt(c):
    return max(c.rts)


t.replace_column("chromatogram", t.apply(cut_chromatogram, t.chromatogram))
t.replace_column("rtmin_chromatogram", t.apply(minrt, t.chromatogram))
t.replace_column("rtmax_chromatogram", t.apply(maxrt, t.chromatogram))

t = integrate_chromatograms(t, "sgolay")

t2 = t.join(t)

inspect(t2)

sys.exit(0)

if not os.path.exists(PEAKS):
    if 0:
        pm = PeakMap.load("experiments/test_smaller.mzXML")
    else:
        pm = PeakMap.load("experiments/ms1_and_ms2_mixed.mzML")
    t = run_feature_finder_metabo(pm)
    attach_ms2_spectra(t)
    t.save(PEAKS)
else:
    t = Table.load(PEAKS)

if 1:
    t = Table.load("experiments/peaks_and_chromatograms_mixed.table")

    t = t.filter(t.chromatogram.is_none())
    # t = t.extract_columns("id", "chromatogram_type", "chromatogram", "mz", "rt")

    t = integrate(t, "sgolay", n_cores=4)
    inspect(t)


if 0:
    print(t.col_names)

    t = t.filter(t.num_spectra_ms2 > 1)
    t.drop_columns("id")
    t.add_enumeration()

    t2 = t.copy()

    t3 = t.join(t2, t.id == t2.id + 1)
    t3.replace_column("id__0", t3.id, int)

    inspect(t3)

if 0:

    def intensity(c):
        return max(c.intensities)

    t.replace_column("intensity", t.apply(intensity, t.chromatogram), float, "%.2e")

    t2 = t.copy()
    t3 = t.join(t2, t.id == t2.id + 1)

    inspect(t3.filter(t3.id.is_in((1, 835))))

if 0:
    t.set_col_format("peakmap", "%s")

    colors = list(matplotlib.colormaps.get_cmap("tab10").colors)

    def color(quality):
        if quality < 1e-5:
            return

        random.shuffle(colors)
        return dict(zip(t.col_names, colors))

        # return "200, 150, 150" if quality > 1e-5 else None

    t.add_column("color", t.apply(color, t.quality), object)
    # t = t.join(t)

    t21 = t[:10].copy()
    t22 = t[:10].copy()
    t3 = t21.join(t22, t21.id == t22.id + 1)

    t0 = emzed.to_table("a", [1, 2, 3], int)
    t0.add_column(
        "color",
        ["#DD6666", None, "#DD6666"],
        object,  # [dict(a="#CC7777", color="#AAAAAA"), dict(a="#7777CC"), None], object
    )

    inspect(t3)
